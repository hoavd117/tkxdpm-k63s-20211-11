package fee;

import controller.FeeController;
import controller.fee.NewCalculateFee;
import entity.common.Bike;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class NewCalculateFeeTest {

	private FeeController feeController;

	@BeforeEach
	void setUp() {
		feeController = new FeeController();
		feeController.setCalculateFee(new NewCalculateFee());
	}

	@ParameterizedTest
	@CsvSource({
			"0, 0, 80000" ,
			"61, 0, 90000" ,
			"121, 0, 100000" ,
			"601, 0, 180000" ,
			"721, 0, 200000" ,
			"900, 0, 200000" ,
			"1440, 0, 200000" ,
			"1441, 0, 202000" ,
			"2880, 0, 392000" ,
			"44640, 0, 5960000" ,
			"525600, 0, 70088000" ,
			"0, 1, 120000" ,
			"61, 1, 135000" ,
			"121, 1, 150000" ,
			"601, 1, 270000" ,
			"721, 1, 300000" ,
			"900, 1, 300000" ,
			"1440, 1, 300000" ,
			"1441, 1, 303000" ,
			"2880, 1, 588000" ,
			"44640, 1, 8940000" ,
			"525600, 1, 105132000" ,
			"0, 1, 120000" ,
			"61, 1, 135000" ,
			"121, 1, 150000" ,
			"601, 1, 270000" ,
			"721, 1, 300000" ,
			"900, 1, 300000" ,
			"1440, 1, 300000" ,
			"1441, 1, 303000" ,
			"2880, 1, 588000" ,
			"44640, 1, 8940000" ,
			"525600, 1, 105132000"
	})

	//@Test
	void test(long totalMinutesRent, int bikeTypeValue, long feeExpected) {
		Bike bike = new Bike();
		bike.setType(bikeTypeValue);

		//when
		long fee = feeController.calculateFeeRent(bike, totalMinutesRent);

		//then
		assertEquals(feeExpected, fee);
	}

}
