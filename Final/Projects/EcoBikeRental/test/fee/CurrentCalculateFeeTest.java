package fee;

import controller.FeeController;
import controller.fee.CurrentCalculateFee;
import entity.common.Bike;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CurrentCalculateFeeTest {

	FeeController feeController;

	@BeforeEach
	void setUp() {
		feeController = new FeeController();
		feeController.setCalculateFee(new CurrentCalculateFee());
	}

	@ParameterizedTest
	@CsvSource({
			"0, 0, 0" ,
			"1, 0, 0" ,
			"2, 0, 0" ,
			"3, 0, 0" ,
			"5, 0, 0" ,
			"8, 0, 0" ,
			"13, 0, 10000" ,
			"21, 0, 10000" ,
			"30, 0, 10000" ,
			"34, 0, 13000" ,
			"180, 0, 40000" ,
			"300, 0, 64000" ,
			"1440, 0, 292000" ,
			"44640, 0, 8932000" ,
			"525600, 0, 105124000" ,
			"0, 1, 0" ,
			"1, 1, 0" ,
			"2, 1, 0" ,
			"3, 1, 0" ,
			"5, 1, 0" ,
			"8, 1, 0" ,
			"13, 1, 15000" ,
			"21, 1, 15000" ,
			"30, 1, 15000" ,
			"34, 1, 19500" ,
			"180, 1, 60000" ,
			"300, 1, 96000" ,
			"1440, 1, 438000" ,
			"44640, 1, 13398000" ,
			"525600, 1, 157686000" ,
			"0, 1, 0" ,
			"1, 1, 0" ,
			"2, 1, 0" ,
			"3, 1, 0" ,
			"5, 1, 0" ,
			"8, 1, 0" ,
			"13, 1, 15000" ,
			"21, 1, 15000" ,
			"30, 1, 15000" ,
			"34, 1, 19500" ,
			"180, 1, 60000" ,
			"300, 1, 96000" ,
			"1440, 1, 438000" ,
			"44640, 1, 13398000" ,
			"525600, 1, 157686000"
	})

	//@Test
	void test(long totalMinutesRent, int bikeTypeValue, long feeExpected) {
		Bike bike = new Bike();
		bike.setType(bikeTypeValue);

		//when
		long fee = feeController.calculateFeeRent(bike, totalMinutesRent);

		//then
		assertEquals(feeExpected, fee);
	}

}
