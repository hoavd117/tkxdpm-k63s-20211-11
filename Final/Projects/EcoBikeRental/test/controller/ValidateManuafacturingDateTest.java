package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ValidateManuafacturingDateTest {

	private ManageBikeController manageBikeController;
	
	@BeforeEach
	void setUp() throws Exception {
		manageBikeController = new ManageBikeController();
	}
	
	@ParameterizedTest
	@CsvSource({
		",true",
		"abcdef,false",
		"1/12/1234,false",
		"01/00/1234,false",
		"01/13/1234,false",
		"00/12/1234,false",
		"32/12/1234,false",
		"01/12/1234,true",
		"29/05/1234,true",
		"29/05/1234,true",
		"29/02/2016,true",
		"29/02/2017,false",
		"30/04/1234,true",
		"30/02/1234,false",
		"31/01/1234,true",
		"31/04/1234,false"
	})

	//@Test
	void test(String date, boolean expected) {
		//when
		boolean isValided = manageBikeController.validateManuafacturingDate(date);
		
		//then
		assertEquals(expected, isValided);
	}

}
