package entity.common;

public class User {
	private Integer userId;
	private String fullName;
	private String phoneNumber;
	private String email;
	private Integer age;
	private Integer gender;
	
	public User() {
		super();
	}
	
	public User(Integer userId, String fullName, String phoneNumber, String email, Integer age, Integer gender) {
		super();
		this.userId = userId;
		this.fullName = fullName;
		this.phoneNumber = phoneNumber;
		this.email = email;
		this.age = age;
		this.gender = gender;
	}
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	
	
}
