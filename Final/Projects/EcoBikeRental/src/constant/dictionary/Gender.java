package constant.dictionary;

import java.util.Objects;

public enum Gender {
	MALE(0), FEMALE(1), OTHER(2);
	
	private int value;
	
	Gender(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return this.value;
	}
	 
	public static Gender getFromValue(int value) {
		for(Gender gender : Gender.values()) {
			if(gender.value == value) {
				return gender;
			}
		}
		return null;
	}
	
	public static Gender getFromName(String name) {
		System.out.println(name);
		for(Gender gender : Gender.values()) {
			if(Objects.equals(name, gender.name())) {
				return gender;
			}
		}
		return null;
	}
}
