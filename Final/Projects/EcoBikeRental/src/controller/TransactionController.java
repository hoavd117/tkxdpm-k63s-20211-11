package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

import connection.DbConnection;
import connection.impl.SqlConnection;
import entity.common.Transaction;
import entity.common.User;

public class TransactionController extends BaseController {
	private DbConnection db = new SqlConnection();
	
	public Transaction getTransaction(int transactionId) {
		Transaction transaction = new Transaction();
		Connection conn = db.getConnection();
		try {
            PreparedStatement statement = conn.prepareStatement("select * from transaction where id = ?");
            statement.setInt(1, transactionId);
            ResultSet rs= statement.executeQuery();
            if (rs.next()) {
            	transaction.setId(rs.getInt("id"));
            	transaction.setUserId(rs.getInt("userId"));
            	transaction.setBikeId(rs.getInt("bikeId"));
//            	System.out.println(transaction.getBikeId());
            	transaction.setCardId(rs.getInt("cardId"));
            	transaction.setStatus(rs.getInt("status"));
            	transaction.setTimeStartRent(rs.getTimestamp("timeStartRent").toInstant());
//            	transaction.setCreatedAt(rs.getDate("createdAt").toInstant());
//            	transaction.setUpdatedAt(rs.getDate("updatedAt").toInstant());
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return transaction;
	}
	
	
	private int userId;
	private int cardId;
	private int status;
	private Instant timeStartRent;
}
