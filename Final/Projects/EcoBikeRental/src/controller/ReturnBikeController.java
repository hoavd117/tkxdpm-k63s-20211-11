package controller;

import entity.common.Bike;
import entity.common.Transaction;

public class ReturnBikeController extends BaseController {
	FeeController feeController = new FeeController();
	ManageBikeController manageBikeController = new ManageBikeController();
	TransactionController transactionController = new TransactionController();
	
	public Bike getBikeInfo(int bikeId) {
		Bike bike =  manageBikeController.getBike(bikeId);
		return bike;
	}
	
	public Transaction getTransanctionInfo(int transactionId) {
		Transaction transaction = transactionController.getTransaction(transactionId);
		System.out.println("trans_id" + transaction.getId());
		return transaction;
	}
	
	public long calculateFeeRent(Bike bike, long totalMinutesRent) {
		return feeController.calculateFeeRent(bike, totalMinutesRent);
	}
}
