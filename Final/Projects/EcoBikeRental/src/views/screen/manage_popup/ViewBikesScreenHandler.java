package views.screen.manage_popup;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.ResourceBundle;

import controller.ManageBikeController;
import entity.common.Bike;
import exception.UnrecognizedException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

/**
 * This class is to handle action on view bikes screen
 * @author Administrator
 *
 */
public class ViewBikesScreenHandler extends BaseScreenHandler implements Initializable {
	@FXML
	private TextField bikeName;
	
	@FXML
	private TextField bikeStationName;
	
	@FXML 
	private TableView<Bike> bikesList;
	
	@FXML
	private Button searchBikeBtn;
	
	@FXML
	private Button updateListBtn;
	
	@FXML
	private Button saveBikeBtn;
	
	@FXML
	private Button deleteBikeBtn;
	
	/**
	 * Constructor
	 * @param stage: this stage
	 * @param screenPath: path to screen
	 * @throws IOException
	 */
	public ViewBikesScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	/**
	 * This method is to get an observable list of all bikes
	 * @return list
	 */
	private ObservableList<Bike> getBikesList() {
		ObservableList<Bike> list = FXCollections.observableArrayList(getBController().getAllBikes());
		return list;
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setBController(new ManageBikeController());
		
		// add all bikes information into table
		TableColumn<Bike, String> nameCol = new TableColumn<Bike, String>();
	    nameCol.setText("Name");
	    nameCol.setMinWidth(120);
	    nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
	    TableColumn<Bike, String> stationNameCol = new TableColumn<Bike, String>();
	    stationNameCol.setText("Station name");
	    stationNameCol.setMinWidth(120);
	    stationNameCol.setCellValueFactory(new PropertyValueFactory<>("stationName"));
	    TableColumn<Bike, String> typeCol = new TableColumn<Bike, String>();
	    typeCol.setText("Type");
	    typeCol.setMinWidth(100);
	    typeCol.setCellValueFactory(new PropertyValueFactory<>("typeStr"));
	    TableColumn<Bike, String> weightCol = new TableColumn<Bike, String>();
	    weightCol.setText("Weight");
	    weightCol.setMinWidth(70);
	    weightCol.setCellValueFactory(new PropertyValueFactory<>("Weight"));
	    TableColumn<Bike, String> licensePlateCol = new TableColumn<Bike, String>();
	    licensePlateCol.setText("License plate");
	    licensePlateCol.setMinWidth(120);
	    licensePlateCol.setCellValueFactory(new PropertyValueFactory<>("licensePlate"));
	    TableColumn<Bike, String> manuafacturingDateCol = new TableColumn<Bike, String>();
	    manuafacturingDateCol.setText("Manuafacturing date");
	    manuafacturingDateCol.setMinWidth(180);
	    manuafacturingDateCol.setCellValueFactory(new PropertyValueFactory<>("manuafacturingDate"));
	    TableColumn<Bike, String> producerCol = new TableColumn<Bike, String>();
	    producerCol.setText("Producer");
	    producerCol.setMinWidth(110);
	    producerCol.setCellValueFactory(new PropertyValueFactory<>("producer"));
	    TableColumn<Bike, String> costCol = new TableColumn<Bike, String>();
	    costCol.setText("Cost");
	    costCol.setMinWidth(80);
	    costCol.setCellValueFactory(new PropertyValueFactory<>("cost"));
	 
	    bikesList.getColumns().addAll(nameCol, stationNameCol, typeCol, weightCol, licensePlateCol, manuafacturingDateCol, producerCol, costCol);
		bikesList.setItems(getBikesList());
		
		bikeName.textProperty().addListener((obs, oldText, newText) -> {
			// if bikeName text field is not empty then disable bikeStationName text field
			if (newText == null || newText.trim().isEmpty()) {
				bikeStationName.setDisable(false);
			}
			else {
				bikeStationName.setDisable(true);
			}
		});
		
		bikeStationName.textProperty().addListener((obs, oldText, newText) -> {
			// if bikeStationName text field is not empty then disable bikeName text field
			if (newText == null || newText.trim().isEmpty()) {
				bikeName.setDisable(false);
			}
			else {
				bikeName.setDisable(true);
			}
		});
		
		searchBikeBtn.setOnMouseClicked(e -> {
			try {
				confirmToSearchBike(); // confirm and process request to search bike
			} catch (SQLException | IOException exp) {
				exp.printStackTrace();
				throw new UnrecognizedException(Arrays.toString(exp.getStackTrace()).replaceAll(", ", "\n"));
			}
		});
		
		updateListBtn.setOnMouseClicked(e -> {
			confirmToUpdateList(); // confirm and process request to update table
		});
		
		saveBikeBtn.setOnMouseClicked(e -> {
			try {
				confirmToSaveBike(); // confirm and process request to save bike information
			} catch (SQLException | IOException exp) {
				exp.printStackTrace();
				throw new UnrecognizedException(Arrays.toString(exp.getStackTrace()).replaceAll(", ", "\n"));
			}
		});
		
		deleteBikeBtn.setOnMouseClicked(e -> {
			try {
				confirmToDeleteBike(); // confirm and process request to delete bike
			} catch (SQLException | IOException exp) {
				exp.printStackTrace();
				throw new UnrecognizedException(Arrays.toString(exp.getStackTrace()).replaceAll(", ", "\n"));
			}
		});
	}
	
	/**
	 * This method is to confirm and process request to search bike
	 * @throws SQLException
	 * @throws IOException
	 */
	private void confirmToSearchBike() throws SQLException, IOException {
		// to do something
	}
	
	/**
	 * This method is to confirm and process request to update list bikes on table
	 * @throws SQLException
	 * @throws IOException
	 */
	private void confirmToUpdateList() {
		bikesList.setItems(getBikesList());
	}
	
	/**
	 * This method is to confirm and process request to save bike information
	 * @throws SQLException
	 * @throws IOException
	 */
	private void confirmToSaveBike() throws SQLException, IOException {
		// to do something
	}
	
	/**
	 * This method is to confirm and process request to delete bike
	 * @throws SQLException
	 * @throws IOException
	 */
	private void confirmToDeleteBike() throws SQLException, IOException {
		String chosenName = bikeName.getText();
		String chosenStationName = bikeStationName.getText();
		
		if (chosenStationName == null || chosenStationName.trim().isEmpty()) {
			getBController().deleteBikeByName(chosenName); // delete bike by name
		}
		
		if (chosenName == null || chosenName.trim().isEmpty()) {
			getBController().deleteBikesByStationName(chosenStationName); // delete bikes by station name
		}
		
		bikesList.setItems(getBikesList());
	}
	
	/**
	 * This method is to get the controller
	 */
	public ManageBikeController getBController(){
		return (ManageBikeController) super.getBController();
	}

}
