package views.screen.manage;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;

import constant.Const;
import controller.ManageBikeController;
import exception.UnrecognizedException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;
import views.screen.manage_popup.AddBikeScreenHandler;
import views.screen.manage_popup.ViewBikesScreenHandler;

/**
 * This class is to handle action on manage bike screen
 * @author Nguyen Dac Thai - 20180172
 *
 */
public class ManageBikeScreenHandler extends BaseScreenHandler {
	@FXML
	private Button addBikeBtn;
	
	@FXML
	private Button viewBikesBtn;
	
	/**
	 * constructor
	 * @param stage: this stage
	 * @param screenPath: path to this screen
	 * @throws IOException
	 */
	public ManageBikeScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
		
		addBikeBtn.setOnMouseClicked(e -> {
			try {
				requestToAddBike(); // start use case add bike
			} catch (SQLException | IOException exp) {
				exp.printStackTrace();
				throw new UnrecognizedException(Arrays.toString(exp.getStackTrace()).replaceAll(", ", "\n"));
			}
		});
		
		viewBikesBtn.setOnMouseClicked(e -> {
			try {
				requestToViewBikes(); // start use case view bikes (update bike + delete bike)
			} catch (SQLException | IOException exp) {
				exp.printStackTrace();
				throw new UnrecognizedException(Arrays.toString(exp.getStackTrace()).replaceAll(", ", "\n"));
			}
		});
		
	}
	
	/**
	 * This method is to process manage bike request
	 * @param screenHandler: screen handler
	 * @param screenTitle: screen title
	 */
	private void processManageBikeRequest(BaseScreenHandler screenHandler, String screenTitle) {
		screenHandler.setPreviousScreen(this);
		screenHandler.setScreenTitle(screenTitle);
		screenHandler.setBController(getBController());
		screenHandler.show();
	}
	
	/**
	 * This method is to process request to add new bike
	 * @throws SQLException
	 * @throws IOException
	 */
	private void requestToAddBike() throws SQLException, IOException {
		processManageBikeRequest(new AddBikeScreenHandler(new Stage(), Const.ScreenPath.ADD_BIKE_SCREEN_PATH), "Add Bike Screen");
	}
	
	/**
	 * This method is to process request to view bikes (update bike + delete bike)
	 * @throws SQLException
	 * @throws IOException
	 */
	private void requestToViewBikes() throws SQLException, IOException {
		processManageBikeRequest(new ViewBikesScreenHandler(new Stage(), Const.ScreenPath.VIEW_BIKES_SCREEN_PATH), "View Bikes Screen");
	}
	
	/**
	 * This method is to notify error message
	 * @param message: error message
	 */
	public void notifyError(String message) {
		//System.out.print(message);
	}
	
	/**
	 * This method is to get the controller
	 */
	public ManageBikeController getBController(){
		return (ManageBikeController) super.getBController();
	}
}
