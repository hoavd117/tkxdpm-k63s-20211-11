package views.screen.manage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.ResourceBundle;

import constant.Const;
import controller.BaseController;
import controller.ManageBikeController;
import exception.UnrecognizedException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

/**
 * This class is to handle action on administrator home screen
 * @author Nguyen Dac Thai - 20180172
 *
 */
public class AdministratorHomeScreenHandler extends BaseScreenHandler implements Initializable {
	@FXML
	private Button manageStationBtn;
	
	@FXML
	private Button manageBikeBtn;
	
	/**
	 * constructor
	 * @param stage: this stage
	 * @param screenPath: path to this screen
	 * @throws IOException
	 */
	public AdministratorHomeScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		setBController(new BaseController());
		
		manageBikeBtn.setOnMouseClicked(e -> {
			try {
				requestToManageBike(); // start use case manage bike
			} catch (SQLException | IOException exp) {
				exp.printStackTrace();
				throw new UnrecognizedException(Arrays.toString(exp.getStackTrace()).replaceAll(", ", "\n"));
			}
		});
	}
	
	/**
	 * This method is to process request to manage bike
	 * @throws SQLException
	 * @throws IOException
	 */
	private void requestToManageBike() throws SQLException, IOException {
		// create and display manage bike screen
		ManageBikeScreenHandler manageBikeScreenHandler = new ManageBikeScreenHandler(this.stage, Const.ScreenPath.MANAGE_BIKE_SCREEN_PATH); 
		manageBikeScreenHandler.setPreviousScreen(this);
		manageBikeScreenHandler.setScreenTitle("Manage Bikes Screen");
		manageBikeScreenHandler.setBController(new ManageBikeController());
		manageBikeScreenHandler.show();
	}
	
}
