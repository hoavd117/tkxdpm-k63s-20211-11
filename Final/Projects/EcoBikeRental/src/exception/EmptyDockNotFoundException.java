package exception;

public class EmptyDockNotFoundException extends BaseException {
	public EmptyDockNotFoundException(String message) {
		super(message);
	}
}
