package exception;;

public class UnrecognizedException extends RuntimeException {
	public UnrecognizedException() {
		super("ERROR: Something went wrong!");
	}
	
	public UnrecognizedException(String e) {
		super("ERROR: Something went wrong!");
	}
}
