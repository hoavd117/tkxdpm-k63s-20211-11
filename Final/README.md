# NHÓM 11 #

### Use case phụ trách của từng bạn ###
* Vũ Đức Hòa - 20173121: Transaction, Manage Fee
* Nguyễn Đức Chính - 20183871: Manage Personal Info
* Nguyễn Đắc Thái - 20180172: Manage Station, Manage Bike
* Đào Thu Hương - 20180096: View Station, View Bike
* Nguyễn Huy Hoàn - 20183900: Return Bike
* Trần Quang Thiện - 20183992: Rent Bike

### Homework 02 ###
* Các thành viên họp và cùng thống nhất vẽ biểu đồ use case tổng quan của hệ thống
* Từng thành viên đặc tả use case mình phụ trách và vẽ biểu đồ hoạt động tương ứng
* Các thành viên họp và cùng ghép các đặc tả use case và biểu đồ hoạt động vào file SRS

Các thành viên
* Vũ Đức Hòa - 20173121: Transaction, Manage Fee
* Nguyễn Đức Chính - 20183871: Manage Personal Info
* Nguyễn Đắc Thái - 20180172: Manage Station, Manage Bike
* Đào Thu Hương - 20180096: View Station, View Bike
* Nguyễn Huy Hoàn - 20183900: Return Bike
* Trần Quang Thiện - 20183992: Rent Bike

### Homework 03 ###
* Từng thành viên vẽ các biểu đồ trình tự của lớp phân tích ứng với use case của mình
* Từng thành viên vẽ các biểu đồ lớp phân tích ứng với use case của mình

Các thành viên
* Vũ Đức Hòa - 20173121: Transaction, Manage Fee
* Nguyễn Đắc Thái - 20180172: Manage Station, Manage Bike
* Nguyễn Huy Hoàn - 20183900: Return Bike
* Trần Quang Thiện - 20183992: Rent Bike

### Homework 04 ###
* Các thành viên họp và thảo luận vẽ sơ đồ dịch chuyển màn hình từ màn hình Home cho đến từng màn hình mỗi thành viên phụ trách, cuộc họp chỉ có Hòa, Thái, Hoàn, Thiện
* Từng thành viên vẽ sơ đồ dịch chuyển màn hình từ màn hình Home đến các màn hình của mình

Các thành viên
* Vũ Đức Hòa - 20173121: Transaction, Manage Fee
* Nguyễn Đức Chính - 20183871: Manage Personal Info
* Nguyễn Đắc Thái - 20180172: Manage Station, Manage Bike
* Nguyễn Huy Hoàn - 20183900: Return Bike
* Trần Quang Thiện - 20183992: Rent Bike

### Homework 05 ###
* Các thành viên họp và thảo luận vẽ biểu đồ gói cho toàn hệ thống cuộc họp chỉ có sự tham gia của Hòa, Thái, Hoàn
* Từng thành viên vẽ các biểu đồ trình tự cho lớp thiết kế ứng với use case của mình
* Từng thành viên vẽ các biểu đồ lớp thiết kế ứng với use case của mình
* Từng thành viên bắt đầu lập trình thiết kế các module theo thiết kế của mình

Các thành viên
* Vũ Đức Hòa - 20173121: Transaction, Manage Fee
* Nguyễn Đức Chính - 20183871: Manage Personal Info
* Nguyễn Đắc Thái - 20180172: Manage Station, Manage Bike
* Nguyễn Huy Hoàn - 20183900: Return Bike

### Homework 06 ###
* Từng thành viên chọn ra một module mình quản lý và tiến hành áp dụng các kỹ thuật kiểm thử để tạo test case cho module đó 
* Từng thành viên lập trình kiểm thử đơn vị với JUnit framework

Các thành viên
* Vũ Đức Hòa - 20173121: Transaction, Manage Fee
* Nguyễn Đắc Thái - 20180172: Manage Station, Manage Bike
* Nguyễn Huy Hoàn - 20183900: Return Bike
* Nguyễn Đức Chính - 20183871: Manage Personal Info

### Homework 07 ###
* Từng thành viên áp dụng các nguyên lý thiết kế và các mẫu thiết kế đã học để chỉnh sửa lại thiết kế chương trình của mình

Các thành viên
* Vũ Đức Hòa - 20173121: Transaction, Manage Fee
* Nguyễn Đắc Thái - 20180172: Manage Station

### Hoàn thiện Project ###

* Hòa, Thái, Hoàn chủ động các cuộc họp, thiết kế phần cơ sở dữ liệu chung, các quy ước về project.
* Hòa, Thái, Hoàn thực hiện việc ghép các chức năng lại với nhau. Hoàn thực hiện thêm màn giao diện Home.
* Hòa, Thái, Hoàn làm báo chung cho cả nhóm hoàn thành bài tập final.

### Phần trăm đóng góp của từng bạn đối với bài tập nhóm ###
* Vũ Đức Hòa - 20173121: 25%
* Nguyễn Đức Chính - 20183871: 15%
* Nguyễn Đắc Thái - 20180172: 25%
* Đào Thu Hương - 20180096: 5%
* Nguyễn Huy Hoàn - 20183900: 25%
* Trần Quang Thiện - 20183992: 5%
