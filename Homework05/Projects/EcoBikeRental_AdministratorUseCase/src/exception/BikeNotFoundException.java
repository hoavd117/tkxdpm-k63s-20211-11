package exception;

public class BikeNotFoundException extends BaseException {
	public BikeNotFoundException(String message) {
		super(message);
	}
}
