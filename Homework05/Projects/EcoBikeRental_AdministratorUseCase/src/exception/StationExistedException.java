package exception;

public class StationExistedException extends BaseException {
	public StationExistedException(String message) {
		super(message);
	}
}
