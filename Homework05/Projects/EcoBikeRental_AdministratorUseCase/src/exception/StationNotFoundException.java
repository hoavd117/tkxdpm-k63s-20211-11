package exception;

public class StationNotFoundException extends BaseException {
	public StationNotFoundException(String message) {
		super(message);
	}
}
