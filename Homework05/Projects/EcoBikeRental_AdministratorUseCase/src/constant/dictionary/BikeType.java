package constant.dictionary;

public enum BikeType {
	SINGLE_NORMAL(0),
	ELECTRIC(1),
	DOUBLE_NORMAL(2);
	
	private int value;
	
	BikeType(int value) {
		this.value = value;
	}
	
	public static BikeType getFromValue(int value) {
		for(BikeType bikeType : BikeType.values()) {
			if(bikeType.value == value) {
				return bikeType;
			}
		}
		return null;
	}
}
