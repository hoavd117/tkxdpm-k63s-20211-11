package connection;

import java.sql.Connection;

public interface DbConnection {
	public Connection getConnection();
}
