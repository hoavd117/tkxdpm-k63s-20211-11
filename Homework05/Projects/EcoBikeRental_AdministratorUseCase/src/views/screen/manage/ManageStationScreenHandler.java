package views.screen.manage;

import java.io.IOException;

import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

public class ManageStationScreenHandler extends BaseScreenHandler {
	public ManageStationScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	public void requestToAddStation() {
		
	}
	
	public void requestToUpdateStation() {
		
	}
	
	public void requestToDeleteStation() {
		
	}
	
	public void confirmToAddStation() {
		
	}
	
	public void confirmToUpdateStation() {
		
	}
	
	public void confirmToDeleteStation() {
		
	}
	
	public void notifyError() {
		
	}
}
