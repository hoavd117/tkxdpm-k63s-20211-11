package views.screen.manage_popup;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ResourceBundle;

import constant.Const;
import controller.ManageBikeController;
import exception.UnrecognizedException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

/**
 * This class is to handle action on add bike screen
 * @author Nguyen Dac Thai - 20180172
 *
 */
public class AddBikeScreenHandler extends BaseScreenHandler implements Initializable {
	@FXML
	private TextField bikeName;
	
	@FXML
	private TextField bikeStationName;
	
	@FXML
	private ComboBox<String> bikeType;
	
	@FXML
	private TextField bikeWeight;
	
	@FXML
	private TextField bikeLicensePlate;
	
	@FXML
	private TextField bikeManuafacturingDate;
	
	@FXML
	private TextField bikeProducer;
	
	@FXML
	private TextField bikeCost;
	
	@FXML
	private TextField bikeBatteryPercentage;
	
	@FXML
	private Button addBikeBtn;
	
	/**
	 * Constructor
	 * @param stage: this stage
	 * @param screenPath: path to this screen
	 * @throws IOException
	 */
	public AddBikeScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.bikeType.getItems().addAll(Const.Items.BIKE_TYPES); // load all bike types to combo box
		this.bikeBatteryPercentage.setDisable(true); // disable until bike type is chosen
		
		addBikeBtn.setOnMouseClicked(e -> {
			try {
				confirmToAddBike(); // confirm and process adding bike information
			} catch (SQLException | IOException exp) {
				exp.printStackTrace();
				throw new UnrecognizedException(Arrays.toString(exp.getStackTrace()).replaceAll(", ", "\n"));
			}
		});
		
		bikeType.getSelectionModel().selectedItemProperty().addListener((options, oldValue, newValue) -> {
			// if bike type is not electricity bike then disable the bikeBatteryPercentage text field 
			if (((String)newValue).equals("Electricity bike")) {
				this.bikeBatteryPercentage.setDisable(false);
			}
			else {
				this.bikeBatteryPercentage.setDisable(true);
			}
		}); 
	}
	
	/**
	 * This method is to confirm and process adding bike information
	 * @throws SQLException
	 * @throws IOException
	 */
	private void confirmToAddBike() throws SQLException, IOException {
		HashMap<String, String> bikeInfo = new HashMap<String, String>();
		
		// add bike information into a hash map
		bikeInfo.put("BIKE_NAME", this.bikeName.getText());
		bikeInfo.put("BIKE_STATION_NAME", this.bikeStationName.getText());
		bikeInfo.put("BIKE_TYPE", this.bikeType.getValue());
		bikeInfo.put("BIKE_WEIGHT", this.bikeWeight.getText());
		bikeInfo.put("BIKE_LICENSE_PLATE", this.bikeLicensePlate.getText());
		bikeInfo.put("BIKE_MANUAFACTURING_DATE", this.bikeManuafacturingDate.getText());
		bikeInfo.put("BIKE_PRODUCER", this.bikeProducer.getText());
		bikeInfo.put("BIKE_COST", this.bikeCost.getText());
		
		if (this.bikeType.getValue() != null) {
			if (this.bikeType.getValue().equals("Electricity bike")) {
				bikeInfo.put("BIKE_BATTERY_PERCENTAGE", this.bikeBatteryPercentage.getText());
			}
		}

		// validate and process bike information
		if (getBController().validateInfo(bikeInfo)) {
			getBController().addBike(bikeInfo);
		}
	}
	
	/**
	 * This method is to get the controller
	 */
	public ManageBikeController getBController(){
		return (ManageBikeController) super.getBController();
	}
}
