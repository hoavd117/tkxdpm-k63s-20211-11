package controller.fee;

import constant.dictionary.BikeType;

public class NewCalculateFee implements ICalculateFee {
    @Override
    public long calculateFee(long totalMinutesRent, BikeType bikeType) {
        long price = calculateFeeWithoutBikeType(totalMinutesRent);

        if(bikeType == BikeType.SINGLE_NORMAL)
            return price;
        if(bikeType == BikeType.DOUBLE_NORMAL || bikeType == BikeType.ELECTRIC){
            return (long)(1.5 * price);
        }
        return 0;
    }

    private long calculateFeeWithoutBikeType(long totalMinutesRent) {
        long price = 200000;

        // Nếu khách hàng trả xe sau khi chỉ thuê xe dưới 12 tiếng, khách hàng sẽ
        // được hoàn tiền 10.000VNĐ mỗi tiếng trả sớm.
        long hour = totalMinutesRent / 60;
        if(hour < 12) {
            return price - (12 - hour) * 10000;
        }

        // Nếu khách hàng trả xe sau khi mượn từ tiếng thứ 12 trở đi, khách hàng
        // sẽ không được hoàn tiền
        if(hour < 24) {
            return price;
        }

        // Nếu khách hàng trả xe muộn quá thời gian thỏa thuận, cứ mỗi 15 phút
        // muộn, khách sẽ phải trả thêm 2.000 đồng.
        totalMinutesRent = totalMinutesRent - 24 * 60;
        int minute15 = (int) Math.ceil(totalMinutesRent / 15.0);
        return price + minute15 * 2000L;
    }
}
