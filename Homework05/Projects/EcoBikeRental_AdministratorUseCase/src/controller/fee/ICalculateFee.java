package controller.fee;

import constant.dictionary.BikeType;

public interface ICalculateFee {
    long calculateFee(long totalMinutesRent, BikeType bikeType);
}
