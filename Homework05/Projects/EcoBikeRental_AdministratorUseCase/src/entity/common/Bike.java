package entity.common;

public class Bike {
	private int id;
	private String name;
	private int type;
	private int weight;
	private String licensePlate;
	private String manuafacturingDate;
	private String producer;
	private int cost;
	private String stationName;
	private int stationId;
	private String bikeDetail;
	private String typeStr;

	public Bike() {
		this.name = "";
		this.type = 0;
		this.weight = 0;
		this.licensePlate = "";
		this.manuafacturingDate = "";
		this.producer = "";
		this.cost = 0;
		this.stationName = "";
	}

	public Bike(String name, int type, int weight, String licensePlate, String manuafacturingDate, String producer, int cost, String stationName) {
		this.name = name;
		this.type = type;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.manuafacturingDate = manuafacturingDate;
		this.producer = producer;
		this.cost = cost;
		this.stationName = stationName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getManuafacturingDate() {
		return manuafacturingDate;
	}

	public void setManuafacturingDate(String manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public String getBikeDetail() {
		return bikeDetail;
	}

	public void setBikeDetail(String bikeDetail) {
		this.bikeDetail = bikeDetail;
	}
	
	public String getTypeStr() {
		return typeStr;
	}

	public void setTypeStr(String typeStr) {
		this.typeStr = typeStr;
	}

}
