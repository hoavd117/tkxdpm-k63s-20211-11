package entity.common;

import java.util.ArrayList;
import java.util.List;

public class Order {
    
    private List listOrderBike;
    private long deposit;

    public Order(){
        this.listOrderBike = new ArrayList<>();
    }

    public Order(List lstOrderMedia) {
        this.listOrderBike = lstOrderMedia;
    }

    public void addBike(Bike om){
        this.listOrderBike.add(om);
    }

    public void removeBike(Bike om){
        this.listOrderBike.remove(om);
    }

    public List getListOrderMedia() {
        return this.listOrderBike;
    }

    public void setListOrderMedia(List lstOrderMedia) {
        this.listOrderBike = lstOrderMedia;
    }

    public long getDeposit() {
        long deposit = 0;
        for (Object object: listOrderBike) {
            Bike bike = (Bike)object;
            deposit += bike.getCost();
        }
        return deposit;
    }
}
