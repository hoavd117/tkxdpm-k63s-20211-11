package entity.manage;

import entity.common.Bike;

/**
 * This class is bike manager entity class
 * @author Nguyen Dac Thai - 20180172
 */
public class ManageBike {
	private final int MAX_NUM_OF_BIKES = (int)1e6; // maximum number of bikes
	private Bike[] bikesList; // list of bikes
	private int numOfBikes; // number of bikes
	
	/**
	 * constructor
	 */
	public ManageBike() {
		bikesList = new Bike[MAX_NUM_OF_BIKES];
		numOfBikes = 0;
	}
	
	/**
	 * This method is to save bike 
	 * @param bike
	 * @return true if successful/false if unsuccessful
	 */
	public boolean saveBike(Bike bike) {
		if (numOfBikes == MAX_NUM_OF_BIKES) return false;
		bikesList[numOfBikes++] = bike;
		return true;
	}
	
	/**
	 * This method is to get a bike by name
	 * @param name: name of a bike
	 * @return bike if found/null if not found
	 */
	public Bike getBikeByName(String name) {
		for (Bike bike: bikesList) {
			if (bike.getName().equals(name)) {
				return bike;
			}
		}
		return null;
	}
	
	/**
	 * This method is to remove a bike by name
	 * @param name: name of a bike
	 * @return true if successful/false if unsuccessful
	 */
	public boolean removeBikeByName(String name) {
		for (int i = 0; i < numOfBikes; ++i) {
			if (bikesList[i].getName().equals(name)) {
				bikesList[i] = bikesList[numOfBikes - 1];
				bikesList[numOfBikes - 1] = null;
				--numOfBikes;
				return true;
			}
		}
		return false;
	}
	
	/**
	 * This method is to get bikes by station name
	 * @param name: station name of bikes
	 * @return chosenBikesList if found/null if not found
	 */
	public Bike[] getBikesByStationName(String name) {
		Bike[] chosenBikesList = new Bike[MAX_NUM_OF_BIKES];
		int numOfChosenBikes = 0;
		for (Bike bike: bikesList) {
			if (bike.getStationName().equals(name)) {
				chosenBikesList[numOfChosenBikes++] = bike;
			}
		}
		if (numOfChosenBikes > 0) return chosenBikesList;
		return null;
	}
	
	/**
	 * This method is to remove bikes by station name
	 * @param name: station name of bikes
	 * @return true if successful/false if unsuccessful
	 */
	public boolean removeBikesByStationName(String name) {
		boolean flag = false;
		for (int i = 0; i < numOfBikes; ++i) {
			if (bikesList[i].getStationName().equals(name)) {
				bikesList[i] = bikesList[numOfBikes - 1];
				bikesList[numOfBikes - 1] = null;
				--numOfBikes;
				--i;
				flag = true;
			}
		}
		return flag;
	}
	
	/**
	 * This method is to get all bikes
	 * @return bikesList
	 */
	public Bike[] getAllBikes() {
		return bikesList;
	}
}
