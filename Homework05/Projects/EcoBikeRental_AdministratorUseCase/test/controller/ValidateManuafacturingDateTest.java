package controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ValidateManuafacturingDateTest {

	private ManageBikeController manageBikeController;
	
	@BeforeEach
	void setUp() throws Exception {
		manageBikeController = new ManageBikeController();
	}
	
	@ParameterizedTest
	@CsvSource({
		"28/07/2000,true",
		"29/02/2016,true",
		",true",
		"01/13/1999,false",
		"32/01/2021,false",
		"31/04/2021,false",
		"30/02/2021,false",
		"29/02/2021,false",
		"28/7/2000,false",
		"1/12/2000,false",
		"12-12-2021,false",
		"abcdef,false",
	})

	//@Test
	void test(String date, boolean expected) {
		//when
		boolean isValided = manageBikeController.validateManuafacturingDate(date);
		
		//then
		assertEquals(expected, isValided);
	}

}
