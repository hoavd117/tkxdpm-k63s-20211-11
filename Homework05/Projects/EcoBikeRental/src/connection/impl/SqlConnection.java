package connection.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

import connection.DbConnection;

public class SqlConnection implements DbConnection {

	private static Connection dbConnection;

    @Override
    public Connection getConnection() {
        if (Objects.isNull(dbConnection)) {
            try {
            	dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/eco_bike_rental?user=root&password=powzxc2000@");
            } catch (SQLException  e) {
                e.printStackTrace();
            }
        }
        return dbConnection;
    }

}
