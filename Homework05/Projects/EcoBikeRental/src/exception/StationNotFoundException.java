package exception;

public class StationNotFoundException extends BaseException {
	private final String MESSAGE = "STATION NOT FOUND!";
	
	@Override
	public String getMessage() {
		return MESSAGE;
	}
}
