package exception;

public class BikeNotFoundException extends BaseException {
	private final String MESSAGE = "BIKE NOT FOUND!";
	
	@Override
	public String getMessage() {
		return MESSAGE;
	}
}
