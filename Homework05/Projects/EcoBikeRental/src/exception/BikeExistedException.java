package exception;

public class BikeExistedException extends BaseException {
	private final String MESSAGE = "BIKE EXISTED!";
	
	@Override
	public String getMessage() {
		return MESSAGE;
	}
}
