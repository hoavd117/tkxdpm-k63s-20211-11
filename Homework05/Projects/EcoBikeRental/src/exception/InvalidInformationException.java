package exception;

public class InvalidInformationException extends BaseException {
	private final String MESSAGE = "INVALID INFORMATION!";
	
	@Override
	public String getMessage() {
		return MESSAGE;
	}
}
