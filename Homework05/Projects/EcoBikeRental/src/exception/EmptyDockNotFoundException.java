package exception;

public class EmptyDockNotFoundException extends BaseException {
	private final String MESSAGE = "EMPTY DOCK NOT FOUND!";
	
	@Override
	public String getMessage() {
		return MESSAGE;
	}
}
