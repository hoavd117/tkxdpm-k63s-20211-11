package exception;

public class StationExistedException extends BaseException {
	private final String MESSAGE = "STATION EXISTED!";
	
	@Override
	public String getMessage() {
		return MESSAGE;
	}
}
