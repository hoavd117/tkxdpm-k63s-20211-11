package exception;

public abstract class BaseException {
	public abstract String getMessage();
}
