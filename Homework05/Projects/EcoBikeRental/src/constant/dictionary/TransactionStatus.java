package constant.dictionary;

public enum TransactionStatus {
	DEPOSIT(0),
	RETURN(1);
	
	private int value;
	
	private TransactionStatus(int value) {
		this.value = value;
	}
	
	public static TransactionStatus getByValue(int value) {
		for(TransactionStatus transactionStatus : TransactionStatus.values()) {
			if(transactionStatus.value ==  value) {
				return transactionStatus;
			}
		}
		return null;
	}
	
	public int getValue() {
		return value;
	}
}
