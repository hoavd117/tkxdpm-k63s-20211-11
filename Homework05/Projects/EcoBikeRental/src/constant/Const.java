package constant;

import java.util.List;

public class Const {
	
	public class ScreenPath{
		public static final String LOGIN_SCREEN_PATH = "/views/screen/login/LoginScreen.fxml";
		
		public static final String HOME_SCREEN_PATH = "/views/screen/home/HomeScreen.fxml";
		public static final String PROFILE_SCREEN_PATH = "/views/screen/profile/ProfileScreen.fxml";
		public static final String RETURN_BIKE_SCREEN_PATH = "/views/screen/return_bike/ReturnBikeScreen.fxml";
		public static final String FEEDBACK_SCREEN_PATH = "/views/screen/return_bike/FeedbackScreen.fxml";
		public static final String PAYMENT_SCREEN_PATH = "/views/screen/payment/PaymentScreen.fxml";
		public static final String RESULT_SCREEN_PATH = "/views/screen/payment/ResultScreen.fxml";
		public static final String MANAGER_FEE_SCREEN_PATH = "/views/screen/manager_fee/ManagerFeeScreen.fxml";

		public static final String ADMINISTRATOR_HOME_SCREEN_PATH = "/views/screen/fxml/administrator_home.fxml";
		public static final String MANAGE_BIKE_SCREEN_PATH = "/views/screen/fxml/manage_bikes.fxml";
		public static final String ADD_BIKE_SCREEN_PATH = "/views/screen/fxml/add_bike.fxml";
		public static final String VIEW_BIKES_SCREEN_PATH = "/views/screen/fxml/view_bikes.fxml";
			
	}
	
	
}
