package views.screen.manager_fee;

import controller.FeeController;
import controller.fee.CurrentCalculateFee;
import controller.fee.ICalculateFee;
import controller.fee.NewCalculateFee;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;
import views.screen.home.HomeScreenHandler;
import views.screen.manage.AdministratorHomeScreenHandler;

import java.io.IOException;
import java.util.Arrays;

import constant.Const;

public class ManagerFeeScreenHandler extends BaseScreenHandler {
    int toggled = 0;

    @FXML
    private Label message;

    @FXML
    private ToggleGroup group;
    @FXML
    private RadioButton btnCurrentCalculateFee;

    @FXML
    private RadioButton btnNewCalculateFee;

    @FXML
    private  Button btnConfirm;

    @FXML
    private  Button btnBack;

    public ManagerFeeScreenHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);

        btnCurrentCalculateFee.setToggleGroup(group);
        btnNewCalculateFee.setToggleGroup(group);

        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                // Có lựa chọn
                if (group.getSelectedToggle() != null) {
                    RadioButton button = (RadioButton) group.getSelectedToggle();
                    if(button == btnCurrentCalculateFee){
                        toggled = 0;
                    } else if(button == btnNewCalculateFee) {
                        toggled = 1;
                    }
                }
            }
        });

        btnConfirm.setOnMouseClicked(e -> {
            try {
                onChangeFeeButtonClick();
            } catch (Exception exp) {
                System.out.println(Arrays.toString(exp.getStackTrace()));
            }
        });
    }

    protected void onChangeFeeButtonClick() {
        System.out.println(toggled);
        if(toggled == 0) {
            FeeController.calculateFee = new CurrentCalculateFee();
            message.setText("Cách tính phí đã đổi sang cách tính hiện tại");
        } else{
        	FeeController.calculateFee = new NewCalculateFee();
            message.setText("Cách tính phí đã đổi sang cách tính mới");

        }
    }
    
    @FXML
	public void backPress() throws IOException {
    	AdministratorHomeScreenHandler administratorHomeScreenHandler = new AdministratorHomeScreenHandler(this.stage, Const.ScreenPath.ADMINISTRATOR_HOME_SCREEN_PATH);
		administratorHomeScreenHandler.setPreviousScreen(this);
		administratorHomeScreenHandler.setScreenTitle("Admin Screen");
		administratorHomeScreenHandler.show();
	}
}

