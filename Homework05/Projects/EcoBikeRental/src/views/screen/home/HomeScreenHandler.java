package views.screen.home;

import java.io.IOException;

import constant.Const;
import controller.ManageBikeController;
import controller.PaymentController;
import controller.ProfileController;
import controller.ReturnBikeController;
import controller.TransactionController;
import entity.common.Transaction;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;
import views.screen.manage.ManageBikeScreenHandler;
import views.screen.manage.ManageStationScreenHandler;
import views.screen.manager_fee.ManagerFeeScreenHandler;
import views.screen.payment.PaymentScreenHandler;
import views.screen.profile.ProfileScreenHandler;
import views.screen.return_bike.ReturnBikeScreen;

public class HomeScreenHandler extends BaseScreenHandler {
	@FXML
	private Text txt_alert;
	@FXML
	private Button btn_returnBike;
	@FXML
	private Button btn_manageProfile;
	@FXML
	private Button btn_payment;

	public HomeScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	@FXML
	public void goToReturnBike() throws IOException {
		Transaction transaction = new TransactionController().getTransaction();
		if(transaction == null) {
			txt_alert.setText("Bạn hiện tại chưa thuê xe nào !");
			return;
		}
		ReturnBikeScreen returnBikeScreen = new ReturnBikeScreen(this.stage, Const.ScreenPath.RETURN_BIKE_SCREEN_PATH, transaction);
		returnBikeScreen.setHomeScreenHandler(this);
        returnBikeScreen.setBController(new ReturnBikeController());
        returnBikeScreen.setPreviousScreen(this);
        returnBikeScreen.setScreenTitle("Return Bike Screen");
        returnBikeScreen.show();
	}
	
	@FXML
	public void goToManageProfile() throws IOException {
		resetAlert();
		ProfileScreenHandler profileScreenHandler = new ProfileScreenHandler(this.stage, Const.ScreenPath.PROFILE_SCREEN_PATH, 1);
		profileScreenHandler.setHomeScreenHandler(this);
		profileScreenHandler.setBController(new ProfileController());
		profileScreenHandler.setPreviousScreen(this);
		profileScreenHandler.setScreenTitle("Profile Screen");
		profileScreenHandler.show();
	}
	
	@FXML
	public void goToPayment() throws IOException {
		resetAlert();
		PaymentScreenHandler paymentScreenHandler = new PaymentScreenHandler(this.stage, Const.ScreenPath.PAYMENT_SCREEN_PATH, 100000, "tra tien thue xe");
		paymentScreenHandler.setHomeScreenHandler(this);
		paymentScreenHandler.setPreviousScreen(this);
		paymentScreenHandler.setScreenTitle("Profile Screen");
		paymentScreenHandler.show();
	}
	
	private void resetAlert() {
		txt_alert.setText("");
	}
}
