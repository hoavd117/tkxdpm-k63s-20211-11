package views.screen.login;

import java.io.IOException;

import constant.Const;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;
import views.screen.home.HomeScreenHandler;
import views.screen.manage.AdministratorHomeScreenHandler;

public class LoginScreenHandler extends BaseScreenHandler {
	@FXML
	private Button btn_loginWithCustomer;
	@FXML
	private Button btn_loginWithAdmin;

	public LoginScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	@FXML
	public void loginWithCustomer() throws IOException {
		HomeScreenHandler homeScreenHandler = new HomeScreenHandler(this.stage, Const.ScreenPath.HOME_SCREEN_PATH);
		homeScreenHandler.setBController(null);
		homeScreenHandler.setPreviousScreen(this);
		homeScreenHandler.setScreenTitle("Home Screen");
		homeScreenHandler.show();
	}
	
	@FXML
	public void loginWithAdmin() throws IOException {
		AdministratorHomeScreenHandler administratorHomeScreenHandler = new AdministratorHomeScreenHandler(this.stage, Const.ScreenPath.ADMINISTRATOR_HOME_SCREEN_PATH);
		administratorHomeScreenHandler.setPreviousScreen(this);
		administratorHomeScreenHandler.setScreenTitle("Admin Screen");
		administratorHomeScreenHandler.show();
	}
}
