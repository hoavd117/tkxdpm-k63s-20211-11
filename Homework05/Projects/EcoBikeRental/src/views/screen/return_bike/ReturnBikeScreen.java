package views.screen.return_bike;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

import constant.Const;
import controller.FeedbackController;
import controller.ReturnBikeController;
import entity.common.Bike;
import entity.common.Transaction;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;
import views.screen.home.HomeScreenHandler;

public class ReturnBikeScreen extends BaseScreenHandler {
	private Transaction transaction;
	private Bike bike;
	private long totalRefund;
	
	@FXML
	Button btnBack;
	@FXML
	Text txtStartTime;
	@FXML
	Text txtReturnTime;
	@FXML
	Text txtTimeRent;
	@FXML
	Text txtFeeRent;
	@FXML
	Text txtDeposit;
	@FXML
	Text txtTotalRefund;
	
	@FXML
	Text txtBikeId;
	@FXML
	Text txtBikeDetail;
	@FXML
	Text txtBikeName;
	
	@FXML
	Button btnReturn;
	
	public ReturnBikeScreen(Stage stage, String screenPath, Transaction transaction) throws IOException {
		super(stage, screenPath);
		this.bController = new ReturnBikeController();
		initData(transaction);
	}
	
	private void initData(Transaction transaction) {
		this.transaction = transaction;
		Bike bike = ((ReturnBikeController)bController).getBikeInfo(transaction.getBikeId());
		
		txtStartTime.setText(transaction.getTimeStartRent().toString());
		txtReturnTime.setText(Instant.now().toString());
		
		long totalTimeRent = Duration.between(transaction.getTimeStartRent(), Instant.now()).toMinutes();
		txtTimeRent.setText(String.valueOf(totalTimeRent) + " minutes");
		
		long feeRent = ((ReturnBikeController)bController).calculateFeeRent(bike, totalTimeRent);
		txtFeeRent.setText(String.valueOf(feeRent) + " VND");
		
		txtDeposit.setText(String.valueOf(bike.getCost() + " VND"));
		totalRefund = bike.getCost() - feeRent;
		if(totalRefund < 0 ) {
			totalRefund = 0;
		}
		txtTotalRefund.setText(String.valueOf(totalRefund) + " VND");
		
		txtBikeId.setText(String.valueOf(bike.getId()));
		txtBikeDetail.setText(bike.getBikeDetail());
		txtBikeName.setText(bike.getName());
	}
	
	
	@FXML
	public void backPress() throws IOException {
		HomeScreenHandler homeScreenHandler = new HomeScreenHandler(this.stage, Const.ScreenPath.HOME_SCREEN_PATH);
		homeScreenHandler.setHomeScreenHandler(this.getHomeScreenHandler());
		homeScreenHandler.setBController(null);
		homeScreenHandler.setPreviousScreen(this);
		homeScreenHandler.setScreenTitle("Home Screen");
		homeScreenHandler.show();
	}
	
	@FXML
	public void returnBike() throws IOException {
		((ReturnBikeController)this.bController).returnBike(transaction, totalRefund);
		FeedbackScreenHandler feedbackScreenHandler = new FeedbackScreenHandler(this.stage, Const.ScreenPath.FEEDBACK_SCREEN_PATH, transaction, totalRefund);
		feedbackScreenHandler.setHomeScreenHandler(this.getHomeScreenHandler());
		feedbackScreenHandler.setBController(new FeedbackController());
		feedbackScreenHandler.setPreviousScreen(this);
		feedbackScreenHandler.setScreenTitle("Feedback Screen");
		feedbackScreenHandler.show();
	}
	
}
