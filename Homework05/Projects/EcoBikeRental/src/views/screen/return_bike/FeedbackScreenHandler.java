package views.screen.return_bike;

import java.io.IOException;

import constant.Const;
import controller.BaseController;
import controller.FeedbackController;
import entity.common.Transaction;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;
import views.screen.home.HomeScreenHandler;

public class FeedbackScreenHandler extends BaseScreenHandler {
	private Transaction transaction;
	private long totalRefund;
	
	@FXML
	private Text txt_refund;
	@FXML
	private Button btn_sendMessage;
	@FXML
	private Button btn_skip;
	@FXML
	private TextField etxt_message;
	

	public FeedbackScreenHandler(Stage stage, String screenPath, Transaction transaction, long totalRefund) throws IOException {
		super(stage, screenPath);
		this.transaction = transaction;
		this.totalRefund = totalRefund;
		initData();
	}
	
	private void initData() {
		this.txt_refund.setText(String.valueOf(totalRefund) + " VND");
	}
	
	@FXML
	public void saveFeedback() throws IOException {
		String message = etxt_message.getText();
		((FeedbackController)this.bController).saveFeedback(transaction.getId(), message);
		skip();
	}
	
	@FXML
	public void skip() throws IOException {
		HomeScreenHandler homeScreenHandler = new HomeScreenHandler(this.stage, Const.ScreenPath.HOME_SCREEN_PATH);
		homeScreenHandler.setHomeScreenHandler(this.getHomeScreenHandler());
		homeScreenHandler.setBController(null);
		homeScreenHandler.setPreviousScreen(this);
		homeScreenHandler.setScreenTitle("Home Screen");
		homeScreenHandler.show();
	}

}
