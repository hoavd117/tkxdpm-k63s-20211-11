package views.screen;

import java.io.IOException;

import controller.BaseController;
import javafx.scene.Scene;
import javafx.stage.Stage;
import views.screen.home.HomeScreenHandler;
import views.screen.manage.AdministratorHomeScreenHandler;

public class BaseScreenHandler extends FXMLScreenHandler {

	protected Scene scene;
	protected BaseScreenHandler prev;
	protected final Stage stage;
	protected HomeScreenHandler homeScreenHandler;
	protected AdministratorHomeScreenHandler administratorHomeScreenHandler;
	protected String screenTitle;
	protected BaseController bController;

	private BaseScreenHandler(String screenPath) throws IOException {
		super(screenPath);
		this.stage = new Stage();
	}

	public Scene getScene() {
		return scene;
	}

	public Stage getStage() {
		return stage;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	public void setPreviousScreen(BaseScreenHandler prev) {
		this.prev = prev;
	}

	public BaseScreenHandler getPreviousScreen() {
		return this.prev;
	}

	public BaseScreenHandler(Stage stage, String screenPath) throws IOException {
		super(screenPath);
		this.stage = stage;
	}

	public void show() {
		this.scene = new Scene(this.content);
		this.stage.setScene(this.scene);
		this.stage.show();
	}

	public void setScreenTitle(String string) {
		this.stage.setTitle(string);
	}

	public void setBController(BaseController bController){
		this.bController = bController;
	}

	public BaseController getBController(){
		return this.bController;
	}
	
	public void setHomeScreenHandler(HomeScreenHandler HomeScreenHandler) {
		this.homeScreenHandler = HomeScreenHandler;
	}
	
	public HomeScreenHandler getHomeScreenHandler() {
		return this.homeScreenHandler;
	}

	public AdministratorHomeScreenHandler getAdministratorHomeScreenHandler() {
		return administratorHomeScreenHandler;
	}

	public void setAdministratorHomeScreenHandler(AdministratorHomeScreenHandler administratorHomeScreenHandler) {
		this.administratorHomeScreenHandler = administratorHomeScreenHandler;
	}
	
	

}
