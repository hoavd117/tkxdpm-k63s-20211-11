package views.screen.profile;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Objects;
import java.util.ResourceBundle;

import org.junit.validator.PublicClassValidator;

import constant.Const;
import constant.dictionary.Gender;
import controller.BaseController;
import controller.ProfileController;
import controller.ReturnBikeController;
import entity.common.User;
import javafx.application.Application;
import javafx.collections.ObservableArray;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;
import views.screen.home.HomeScreenHandler;
import views.screen.return_bike.ReturnBikeScreen;

public class ProfileScreenHandler extends BaseScreenHandler{
	private User user;
	
	@FXML
	private Button btnBack;
	@FXML
	private Button btnSave;
	@FXML
	private Text txtUserId;
	@FXML
	private TextField etxtFullname;
	@FXML
	private TextField etxtPhoneNumber;
	@FXML
	private TextField etxtEmail;
	@FXML
	private TextField etxtAge;
	@FXML
	private ComboBox<String> cbGender;
	
	public ProfileScreenHandler(Stage stage, String screenPath, int userId) throws IOException {
		super(stage, screenPath);
		this.bController = (BaseController) new ProfileController();
		initProfileData(userId);
	}
	
	private void initProfileData(int userId) {
		this.user = ((ProfileController)this.getBController()).getProfile(userId);
		
		txtUserId.setText(String.valueOf(user.getUserId()));
		etxtFullname.setText(user.getFullName());
		etxtPhoneNumber.setText(user.getPhoneNumber());
		etxtEmail.setText(user.getEmail());
		etxtAge.setText(String.valueOf(user.getAge()));	
		cbGender.setValue(Gender.getFromValue(user.getGender()).name());
		cbGender.getItems().addAll("MALE", "FEMALE", "OTHER");
	}
	
	@FXML
	public void backPress() throws IOException {
		HomeScreenHandler homeScreenHandler = new HomeScreenHandler(this.stage, Const.ScreenPath.HOME_SCREEN_PATH);
		homeScreenHandler.setHomeScreenHandler(this.getHomeScreenHandler());
		homeScreenHandler.setBController(null);
		homeScreenHandler.setPreviousScreen(this);
		homeScreenHandler.setScreenTitle("Home Screen");
		homeScreenHandler.show();
	}
	
	@FXML
	public void saveProfile() throws IOException {
		String fullName = etxtFullname.getText();
		String phoneNumber = etxtPhoneNumber.getText().replace(" ", "");
		String email = etxtEmail.getText().replace(" ", "");
		int age = Integer.parseInt(etxtAge.getText().replace(" ", ""));
		int gender = Gender.getFromName(cbGender.getValue()).getValue();
		
		User userUpdate = new User(user.getUserId(), fullName, phoneNumber, email, age, gender);
		
		if(((ProfileController)this.bController).validateProfile(userUpdate)) {
			((ProfileController)this.bController).saveProfile(userUpdate);
		}
		
		backPress();
		
	}
}
