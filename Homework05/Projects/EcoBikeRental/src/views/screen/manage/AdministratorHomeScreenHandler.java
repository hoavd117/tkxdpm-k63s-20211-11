package views.screen.manage;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.ResourceBundle;

import constant.Const;
import controller.ManageBikeController;
import exception.UnrecognizedException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;
import views.screen.manager_fee.ManagerFeeScreenHandler;

/**
 * This class is to handle action on administrator home screen
 * @author Nguyen Dac Thai - 20180172
 *
 */
public class AdministratorHomeScreenHandler extends BaseScreenHandler {
	
	@FXML
	private Button manageBikeBtn;
	
	@FXML
	private Button manageFeeBtn;
	
	/**
	 * constructor
	 * @param stage: this stage
	 * @param screenPath: path to this screen
	 * @throws IOException
	 */
	public AdministratorHomeScreenHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath);
	}
	
	@FXML
	private void requestToManageBike() throws SQLException, IOException {
		ManageBikeScreenHandler manageBikeScreenHandler = new ManageBikeScreenHandler(this.stage, Const.ScreenPath.MANAGE_BIKE_SCREEN_PATH); 
		manageBikeScreenHandler.setPreviousScreen(this);
		manageBikeScreenHandler.setScreenTitle("Manage Bikes Screen");
		manageBikeScreenHandler.setBController(new ManageBikeController());
		manageBikeScreenHandler.show();
	}
	
	@FXML
	public void goToManageFee() throws IOException {
		ManagerFeeScreenHandler managerFeeScreenHandler = new ManagerFeeScreenHandler(this.stage, Const.ScreenPath.MANAGER_FEE_SCREEN_PATH);
		managerFeeScreenHandler.setAdministratorHomeScreenHandler(this);
		managerFeeScreenHandler.setPreviousScreen(this);
		managerFeeScreenHandler.setScreenTitle("Manage Fee Calculation Screen");
		managerFeeScreenHandler.show();
	}
	
}
