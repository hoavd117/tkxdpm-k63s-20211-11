package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.platform.commons.util.StringUtils;

import connection.DbConnection;
import connection.impl.SqlConnection;
import constant.dictionary.Gender;
import entity.common.User;

public class ProfileController extends BaseController {
	private DbConnection db = new SqlConnection();
	
	public User getProfile(Integer userId) {
		User user = new User();
		Connection conn = db.getConnection();
		try {
            PreparedStatement statement = conn.prepareStatement("select * from user where id = ?");
            statement.setInt(1, userId);
            ResultSet rs= statement.executeQuery();
            if (rs.next()) {
                user.setUserId(rs.getInt("id"));
                user.setFullName(rs.getString("full_name"));
                user.setPhoneNumber(rs.getString("phone_number"));
                user.setEmail(rs.getString("email"));
                user.setAge(rs.getInt("age"));
                user.setGender(rs.getInt("gender"));
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return user;
	}
	
	public void saveProfile(User user) {
		Connection conn = db.getConnection();
		try {
            PreparedStatement statement = conn.prepareStatement("update user set full_name = ?, phone_number = ?, email= ?, age= ?, gender = ? where id = ?");
            statement.setString(1, user.getFullName());
            statement.setString(2, user.getPhoneNumber());
            statement.setString(3, user.getEmail());
            statement.setInt(4, user.getAge());
            statement.setInt(5, user.getGender());
            statement.setInt(6, user.getUserId());
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	public boolean validateProfile(User user) { 
		boolean isValid = validateFullName(user.getFullName()) &&
				validatePhoneNumber(user.getPhoneNumber()) && 
				validateEmail(user.getEmail()) &&
				validateAge(user.getAge());
		if(isValid) return true;
		else {
			System.out.println("not valid");
			return false;
		}
	}
	
	public boolean validateFullName(String fullName) { 
		if(Objects.isNull(fullName) || fullName.isEmpty()) {
			System.out.println("fullname not valid");
			return false;
		}
		return fullName.matches("[a-zA-Z]+");
	}
	
	public boolean validatePhoneNumber(String phoneNumber) { 
		if(Objects.isNull(phoneNumber) || phoneNumber.isEmpty()) {
			System.out.println("phonenumber not valid");
			return false;
		}
		if(phoneNumber.toCharArray()[0] != '0') {
			System.out.println("phonenumber not 0");
			return false;
		}
		if(phoneNumber.length() != 10) {
			System.out.println(phoneNumber.length());
			return false;
		}
		Pattern p = Pattern.compile("[0-9]+");
		Matcher m = p.matcher(phoneNumber);
		return m.matches();
	}
	
	public boolean validateAge(int age) { 
		if(age < 10) {
			System.out.println("age not valid");
			return false;
		}
		if(age > 80) {
			System.out.println("age not valid");
			return false;
		}
		
		return true;
	}
	
	public boolean validateEmail(String email) { 
		if(Objects.isNull(email) || email.isEmpty()) {
			System.out.println("email not valid");
			return false;
		}
		String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
		return email.matches(regex);
	}
}
