package controller;

import constant.dictionary.BikeType;
import controller.fee.CurrentCalculateFee;
import controller.fee.ICalculateFee;
import entity.common.Bike;

public class FeeController extends BaseController {
	public static ICalculateFee calculateFee = new CurrentCalculateFee();

	public void setCalculateFee(ICalculateFee calculateFee) {
		this.calculateFee = calculateFee;
	}

	public long calculateFeeRent(Bike bike, long totalMinutesRent) {
		return calculateFee.calculateFee(totalMinutesRent, BikeType.getFromValue(bike.getType()));
	}
}
