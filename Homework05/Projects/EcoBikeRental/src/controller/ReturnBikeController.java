package controller;

import constant.dictionary.BikeType;
import controller.fee.CurrentCalculateFee;
import controller.fee.ICalculateFee;
import entity.common.Bike;
import entity.common.Transaction;

public class ReturnBikeController extends BaseController {
	ICalculateFee iCalculateFee = new CurrentCalculateFee();
	ManageBikeController manageBikeController = new ManageBikeController();
	TransactionController transactionController = new TransactionController();
	PaymentController paymentController = new PaymentController();
	
	public Bike getBikeInfo(int bikeId) {
		Bike bike =  manageBikeController.getBike(bikeId);
		return bike;
	}
	
	public Transaction getTransanctionInfo(int transactionId) {
		Transaction transaction = transactionController.getTransaction();
		System.out.println("trans_id" + transaction.getId());
		return transaction;
	}
	
	public long calculateFeeRent(Bike bike, long totalMinutesRent) {
		return iCalculateFee.calculateFee(totalMinutesRent, BikeType.getFromValue(bike.getType()));
	}
	
	public void returnBike(Transaction transaction, long totalRefund) {
		transactionController.completeTransaction(transaction.getId());
	}
	
//	public boolean validateTotalRefund
	
//	public void returnBike() {
//		paymentController.
//	}
}
