package controller;

import java.util.Objects;

import connection.DbConnection;
import connection.impl.SqlConnection;

public class FeedbackController extends BaseController {
	private DbConnection db = new SqlConnection();
	
	public void saveFeedback(int transactionId, String message) {
		
	}
	
	public boolean validateMessage(String message) {
		if(Objects.isNull(message) || message.isEmpty()) {
			System.out.println("message not valid");
			return false;
		}
		return message.replace(" ", "").matches("[a-zA-Z0-9]+");
	}
}
