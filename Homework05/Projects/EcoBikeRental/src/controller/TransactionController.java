package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;

import connection.DbConnection;
import connection.impl.SqlConnection;
import constant.dictionary.TransactionStatus;
import entity.common.Transaction;
import entity.common.User;

public class TransactionController extends BaseController {
	private DbConnection db = new SqlConnection();
	
	public Transaction getTransaction() {
		Transaction transaction = null;
		Connection conn = db.getConnection();
		try {
            PreparedStatement statement = conn.prepareStatement("select * from transaction where status = 0");
            ResultSet rs= statement.executeQuery();
            if (rs.next()) {
            	transaction = new Transaction();
            	transaction.setId(rs.getInt("id"));
            	transaction.setUserId(rs.getInt("userId"));
            	transaction.setBikeId(rs.getInt("bikeId"));
//            	System.out.println(transaction.getBikeId());
            	transaction.setCardId(rs.getInt("cardId"));
            	transaction.setStatus(rs.getInt("status"));
            	transaction.setTimeStartRent(rs.getTimestamp("timeStartRent").toInstant());
//            	transaction.setCreatedAt(rs.getDate("createdAt").toInstant());
//            	transaction.setUpdatedAt(rs.getDate("updatedAt").toInstant());
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
		return transaction;
	}
	
	public void completeTransaction(int transactionId) {
		Transaction transaction = new Transaction();
		Connection conn = db.getConnection();
		try {
            PreparedStatement statement = conn.prepareStatement("update transaction set transaction.status= ? where transaction.id = ?");
            statement.setInt(1, TransactionStatus.RETURN.getValue());
            statement.setInt(2, transactionId);
            statement.execute();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
	
	
	private int userId;
	private int cardId;
	private int status;
	private Instant timeStartRent;
}
