package utils;

/**
 * @author hoavd Contains the configs for Ecobike Rental Project
 */
public class Configs {

	// api constants
	public static final String PROCESS_TRANSACTION_URL = "https://ecopark-system-api.herokuapp.com/api/card/processTransaction";
}
