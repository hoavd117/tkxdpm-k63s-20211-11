package entity.common;

public class ElectricityBike extends Bike {
	private int batteryPercentage;
	private int loadCycles;
	private int usageTime;
	
	public ElectricityBike() {
		super();
		this.batteryPercentage = 0;
		this.loadCycles = 0;
		this.usageTime = 0;
	}
}
