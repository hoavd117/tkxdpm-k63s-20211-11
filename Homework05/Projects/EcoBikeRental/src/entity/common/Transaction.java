package entity.common;

import java.time.Instant;

public class Transaction extends AbstractEntity{
	private int id;
	private int userId;
	private int bikeId;
	private int cardId;
	private int status;
	private Instant timeStartRent;
	
	public Transaction() {
		super();
	}
	
	public Transaction(int id, int userId, int bikeId, int cardId, int status, Instant timeStartRent) {
		super();
		this.id = id;
		this.userId = userId;
		this.bikeId = bikeId;
		this.cardId = cardId;
		this.status = status;
		this.timeStartRent = timeStartRent;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public int getBikeId() {
		return bikeId;
	}

	public void setBikeId(int bikeId) {
		this.bikeId = bikeId;
	}

	public int getCardId() {
		return cardId;
	}
	public void setCardId(int cardId) {
		this.cardId = cardId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Instant getTimeStartRent() {
		return timeStartRent;
	}
	public void setTimeStartRent(Instant timeStartRent) {
		this.timeStartRent = timeStartRent;
	}
	
	
}
