package main;

import constant.Const;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import views.screen.home.HomeScreenHandler;
import views.screen.login.LoginScreenHandler;
import views.screen.manager_fee.ManagerFeeScreenHandler;
import views.screen.payment.PaymentScreenHandler;
import views.screen.return_bike.ReturnBikeScreen;

public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Login");
        LoginScreenHandler loginScreenHandler = new LoginScreenHandler(primaryStage, Const.ScreenPath.LOGIN_SCREEN_PATH);
        loginScreenHandler.show();
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}
