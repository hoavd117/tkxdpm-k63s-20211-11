package interbank;

import entity.common.CreditCard;
import exception.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import subsystem.InterbankInterface;
import subsystem.InterbankSubsystem;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RefundTest {
    private InterbankInterface interbank;

    @BeforeEach
    void setUp() {
        interbank = new InterbankSubsystem();
    }

    @ParameterizedTest
    @CsvSource({
            "kscq2_group11_2021, Group 11, 189, 1125, 76, 00",
            "kscq2_group11_2021, Group 11, 189, 1123, 70, 01",
            "kscq2_group11_2021, Group 11, 189, 1125, -10, 07",
    })

        //@Test
    void test(String cardCode, String owner, int cvvCode, String dateExpired, int amount, String errorCodeExpired) {

        CreditCard card = new CreditCard(cardCode, owner, cvvCode, dateExpired);

        Exception exception = new Exception();

        //when
        try {
            interbank.refund(card, amount, "Refund");
        } catch (PaymentException | UnrecognizedException ex) {
            exception = ex;
        }

        //then
        String errorCode = getErrorCodeFromMessage(exception);
        assertEquals(errorCodeExpired, errorCode);
    }

    String getErrorCodeFromMessage(Exception exception) {
        if(exception instanceof InvalidCardException){
            return "01";
        }
        if(exception instanceof NotEnoughBalanceException){
            return "02";
        }
        if(exception instanceof InternalServerErrorException){
            return "03";
        }
        if(exception instanceof SuspiciousTransactionException){
            return "04";
        }
        if(exception instanceof NotEnoughTransactionInfoException){
            return "05";
        }
        if(exception instanceof InvalidVersionException){
            return "06";
        }
        if(exception instanceof InvalidTransactionAmountException){
            return "07";
        }
        if(exception instanceof UnrecognizedException){
            return "";
        }
        return "00";
    }
}
