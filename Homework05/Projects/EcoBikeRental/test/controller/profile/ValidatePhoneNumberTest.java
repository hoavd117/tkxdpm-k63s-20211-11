package controller.profile;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ProfileController;

public class ValidatePhoneNumberTest {
private ProfileController profileController;
	
	@BeforeEach
	void setUp() throws Exception {
		profileController = new ProfileController();
	}
	
	@ParameterizedTest
	@CsvSource({
		"0329333964,true",
		"1236468955,false",
		"012364689555,false",
		"0123646,false",
		"  ,false",
		"Chinh0329333964,false",
		"#0329333964,false",
	})

	//@Test
	void test(String phoneNumber, boolean expected) {
		//when
		boolean isValided = profileController.validatePhoneNumber(phoneNumber);
		
		//then
		assertEquals(expected, isValided);
	}
}
