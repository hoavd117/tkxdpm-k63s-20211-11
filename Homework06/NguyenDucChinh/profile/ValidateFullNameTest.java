package controller.profile;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ProfileController;

public class ValidateFullNameTest {
	private ProfileController profileController;
	
	@BeforeEach
	void setUp() throws Exception {
		profileController = new ProfileController();
	}
	
	@ParameterizedTest
	@CsvSource({
		"NguyenDucChinh,true",
		"  ,false",
		"NguyenDucChinh#@,false",
		"NguyenDucChinh1603,false",
	})

	//@Test
	void test(String fullName, boolean expected) {
		//when
		boolean isValided = profileController.validateFullName(fullName);
		
		//then
		assertEquals(expected, isValided);
	}
}
