package controller.profile;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ProfileController;

public class ValidateAgeTest {
private ProfileController profileController;
	
	@BeforeEach
	void setUp() throws Exception {
		profileController = new ProfileController();
	}
	
	@ParameterizedTest
	@CsvSource({
		"9,false",
		"22,true",
		"91,false",
	})

	//@Test
	void test(int age, boolean expected) {
		//when
		boolean isValided = profileController.validateAge(age);
		
		//then
		assertEquals(expected, isValided);
	}
}
