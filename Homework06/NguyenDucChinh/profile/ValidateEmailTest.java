package controller.profile;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ProfileController;

public class ValidateEmailTest {
private ProfileController profileController;
	
	@BeforeEach
	void setUp() throws Exception {
		profileController = new ProfileController();
	}
	
	@ParameterizedTest
	@CsvSource({
		"abc@gmail.com,true",
		"  ,false",
		"NguyenDucChinh,false",
		"NguyenDucChinh#gmail.com,false",
		"NguyenDucChinh@,false",
		"@gmail.com,false",
	})

	//@Test
	void test(String email, boolean expected) {
		//when
		boolean isValided = profileController.validateEmail(email);
		
		//then
		assertEquals(expected, isValided);
	}
}
