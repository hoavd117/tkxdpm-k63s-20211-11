package subsystem.interbank;

import exception.UnrecognizedException;
import utils.API;

public class InterbankBoundary {

	String query(String data) {
		String response;
		try {
			response = API.post(utils.Configs.PROCESS_TRANSACTION_URL, data);
		} catch (Exception e) {
			e.printStackTrace();
			throw new UnrecognizedException();
		}
		return response;
	}

}
