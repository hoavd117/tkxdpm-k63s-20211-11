package subsystem.interbank;

import entity.common.CreditCard;
import entity.common.PaymentTransaction;
import exception.*;
import utils.MyMap;
import utils.Utils;

import java.util.Map;
import java.util.Objects;

public class InterbankSubsystemController {

	private static final String PUBLIC_KEY = "CmkauBjpZi4=";
	private static final String SECRET_KEY = "B6bUJmi4n6g=";
	private static final String PAY_COMMAND = "pay";
	private static final String REFUND_COMMAND = "refund";
	private static final String VERSION = "1.0.1";

	private static final InterbankBoundary interbankBoundary = new InterbankBoundary();

	private String generateData(Map<String, Object> data) {
		return ((MyMap) data).toJSON();
	}

	public PaymentTransaction payOrder(CreditCard card, int amount, String contents) {
		Map<String, Object> transaction = new MyMap();

		try {
			transaction.putAll(MyMap.toMyMap(card));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new InvalidCardException();
		}
		transaction.put("command", PAY_COMMAND);
		transaction.put("transactionContent", contents);
		transaction.put("amount", amount);
		transaction.put("createdAt", Utils.getToday());

		Map<String, Object> a = new MyMap();
		a.put("secretKey", SECRET_KEY);
		a.put("transaction", transaction);

		Map<String, Object> requestMap = new MyMap();
		requestMap.put("version", VERSION);
		requestMap.put("transaction", transaction);
		requestMap.put("appCode", PUBLIC_KEY);
		requestMap.put("hashCode", Utils.md5(generateData(a)));

		String responseText = interbankBoundary.query(generateData(requestMap));
		MyMap response;
		try {
			response = MyMap.toMyMap(responseText, 0);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			throw new UnrecognizedException();
		}

		return makePaymentTransaction(response);
	}

	public PaymentTransaction refund(CreditCard card, int amount, String contents) {
		Map<String, Object> transaction = new MyMap();

		try {
			transaction.putAll(MyMap.toMyMap(card));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new InvalidCardException();
		}
		transaction.put("command", REFUND_COMMAND);
		transaction.put("transactionContent", contents);
		transaction.put("amount", amount);
		transaction.put("createdAt", Utils.getToday());

		Map<String, Object> hashJson = new MyMap();
		hashJson.put("secretKey", SECRET_KEY);
		hashJson.put("transaction", transaction);

		Map<String, Object> requestMap = new MyMap();
		requestMap.put("version", VERSION);
		requestMap.put("transaction", transaction);
		requestMap.put("appCode", PUBLIC_KEY);
		requestMap.put("hashCode", Utils.md5(generateData(hashJson)));

		String responseText = interbankBoundary.query(generateData(requestMap));
		MyMap response;
		try {
			response = MyMap.toMyMap(responseText, 0);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			throw new UnrecognizedException();
		}

		return makePaymentTransaction(response);
	}


	private PaymentTransaction makePaymentTransaction(MyMap response) {
		if (response == null)
			return null;
		PaymentTransaction trans;
		String errorCode = (String) response.get("errorCode");
		if(Objects.equals(errorCode, "00")){
			MyMap transaction = (MyMap) response.get("transaction");
			CreditCard card = new CreditCard((String) transaction.get("cardCode"), (String) transaction.get("owner"),
					Integer.parseInt((String) transaction.get("cvvCode")), (String) transaction.get("dateExpired"));
			trans = new PaymentTransaction((String) response.get("errorCode"), card,
					(String) transaction.get("transactionId"), (String) transaction.get("transactionContent"),
					Integer.parseInt((String) transaction.get("amount")), (String) transaction.get("createdAt"));
		} else {
			trans = new PaymentTransaction(errorCode);
			switch (errorCode) {
				case "01":
					throw new InvalidCardException();
				case "02":
					throw new NotEnoughBalanceException();
				case "03":
					throw new InternalServerErrorException();
				case "04":
					throw new SuspiciousTransactionException();
				case "05":
					throw new NotEnoughTransactionInfoException();
				case "06":
					throw new InvalidVersionException();
				case "07":
					throw new InvalidTransactionAmountException();
				default:
					throw new UnrecognizedException();
			}
		}


		return trans;
	}

}
