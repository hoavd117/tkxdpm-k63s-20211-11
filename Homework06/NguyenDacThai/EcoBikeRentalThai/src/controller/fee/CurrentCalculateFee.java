package controller.fee;

import constant.dictionary.BikeType;

public class CurrentCalculateFee implements ICalculateFee {

    @Override
    public long calculateFee(long totalMinutesRent, BikeType bikeType) {
        long price = calculateFeeWithoutBikeType(totalMinutesRent);

        if(bikeType == BikeType.SINGLE_NORMAL)
            return price;
        if(bikeType == BikeType.DOUBLE_NORMAL || bikeType == BikeType.ELECTRIC){
            return (long)(1.5 * price);
        }
        return 0;
    }

    private long calculateFeeWithoutBikeType(long totalMinutesRent) {
        // Nếu 10 phút đều thì miễn phí
        if(totalMinutesRent < 10)
            return 0;

        // Giá khởi điểm cho 30 phút đầu là 10.000 đồng
        if(totalMinutesRent <= 30){
            return 10000;
        }

        // Cứ mỗi 15 phút tiếp theo, khách sẽ phải trả thêm 3.000 đồng
        totalMinutesRent = totalMinutesRent - 30;
        int minute15 = (int) Math.ceil(totalMinutesRent / 15.0);
        return 10000 + 3000L * minute15;
    }
}
