package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import connection.DbConnection;
import connection.impl.SqlConnection;
import entity.common.Bike;
import entity.manage.ManageBike;

/**
 * This class controls the flow of manage bike use case
 * @author Nguyen Dac Thai - 20180172
 */
public class ManageBikeController extends BaseController {
	/**
	 * Database
	 */
	private DbConnection db = new SqlConnection();

	/**
	 * Bike manager entity
	 */
	private ManageBike bikesManager;

	public ManageBikeController() {
		bikesManager = new ManageBike();
	}
	
	/**
	 * This method is to add new bike
	 * @param bikeInfo: information of new bike
	 */
	public void addBike(HashMap<String, String> bikeInfo) {
		// extract the information from hash map
		String name = bikeInfo.get("BIKE_NAME");

		int type;
		String typeStr = bikeInfo.get("BIKE_TYPE");
		if (typeStr.equals("Normal bike")) type = 0;
		else if (typeStr.equals("Electricity bike")) type = 1;
		else type = 2;

		int weight = Integer.parseInt(bikeInfo.get("BIKE_WEIGHT"));
		String licensePlate = bikeInfo.get("BIKE_LICENSE_PLATE");
		String manuafacturingDate = bikeInfo.get("BIKE_MANUAFACTURING_DATE");
		String producer = bikeInfo.get("BIKE_PRODUCER");
		int cost = Integer.parseInt(bikeInfo.get("BIKE_COST"));
		String stationName = bikeInfo.get("BIKE_STATION_NAME");

		// create new Bike object and add new one
		Bike bike = new Bike(name, type, weight, licensePlate, manuafacturingDate, producer, cost, stationName);
		bike.setTypeStr(typeStr);
		bikesManager.saveBike(bike);
	}
	
	/**
	 * This method is to update information of a bike
	 * @param bikeInfo: updating information of a bike
	 */
	public void updateBike(HashMap<String, String> bikeInfo) {
		// extract the information from hash map
		String name = bikeInfo.get("BIKE_NAME");
		
		int type;
		String typeStr = bikeInfo.get("BIKE_TYPE");
		if (typeStr.equals("Normal bike")) type = 0;
		else if (typeStr.equals("Electricity bike")) type = 1;
		else type = 2;
		
		int weight = Integer.parseInt(bikeInfo.get("BIKE_WEIGHT"));
		String licensePlate = bikeInfo.get("BIKE_LICENSE_PLATE");
		String manuafacturingDate = bikeInfo.get("BIKE_MANUAFACTURING_DATE");
		String producer = bikeInfo.get("BIKE_PRODUCER");
		int cost = Integer.parseInt(bikeInfo.get("BIKE_COST"));
		String stationName = bikeInfo.get("BIKE_STATION_NAME");
		
		// find chosen bike and update the information of it 
		Bike bike = bikesManager.getBikeByName(name);
		bike.setType(type);
		bike.setWeight(weight);
		bike.setLicensePlate(licensePlate);
		bike.setManuafacturingDate(manuafacturingDate);
		bike.setProducer(producer);
		bike.setCost(cost);
		bike.setStationName(stationName);
		bike.setTypeStr(typeStr);
	}
	
	/**
	 * This method is to delete a bike by name
	 * @param name: name of chosen bike
	 */
	public void deleteBikeByName(String name) {
		bikesManager.removeBikeByName(name);
	}
	
	/**
	 * This method is to delete bikes by station name
	 * @param name: station name of chosen bikes
	 */
	public void deleteBikesByStationName(String name) {
		bikesManager.removeBikesByStationName(name);
	}
	
	/**
	 * This method is to get all bikes
	 * @return all bikes
	 */
	public Bike[] getAllBikes() {
		return bikesManager.getAllBikes();
	}
	
	/**
	 * This method is to validate information of a bike
	 * @param bikeInfo: information of a bike
	 * @return true/false
	 */
	public boolean validateInfo(HashMap<String, String> bikeInfo) {
		String weight = bikeInfo.get("BIKE_WEIGHT");
		if (!validateWeight(weight)) return false;
		
		String licensePlate = bikeInfo.get("BIKE_LICENSE_PLATE");
		if (!validateLicensePlate(licensePlate)) return false;
		
		String manuafacturingDate = bikeInfo.get("BIKE_MANUAFACTURING_DATE");
		if (!validateManuafacturingDate(manuafacturingDate)) return false;
		
		String cost = bikeInfo.get("BIKE_COST");
		if (!validateCost(cost)) return false;
		
		return true;
	}
	
	/**
	 * This method is to validate weight information of a bike
	 * @param weight: weight information of a bike
	 * @return true/false
	 */
	public boolean validateWeight(String weight) {
		//check the weight contains only number
    	try {
    		Integer.parseInt(weight);
    	} catch (NumberFormatException e) {
    		return false;
    	}
		return true;
	}
	
	/**
	 * This method is to validate license plate information of a bike
	 * @param licensePlate: license plate information of a bike
	 * @return true/false
	 */
	public boolean validateLicensePlate(String licensePlate) {
		//check the licensePlate has ' ' character
		if (licensePlate.contains(" ")) return false;
		return true;
	}
	
	/**
	 * This method is to validate manuafacturing date information of a bike
	 * @param manuafacturingDate: manuafacturing date information of a bike
	 * @return true/false
	 */
	public boolean validateManuafacturingDate(String date) {
		//check the date is null
    	if (date == null) return true; //allow null value for manuafacturing date
    	
    	/*
    	 * check the date has correct form: "dd/MM/yyyy"
    	 * 01 <= MM <= 12
    	 * if MM == 01, 03, 05, 07, 08, 10, 12: 01 <= dd <= 31
    	 * if MM == 04, 06, 09, 11: 01 <= dd <= 30
    	 * if MM == 02: 01 <= dd <= 29 if yyyy is leaf year, 01 <= dd <= 28 if yyyy is normal year 
    	 */
    	if (date.matches("[0-9]{2}[/]{1}[0-9]{2}[/]{1}[0-9]{4}")) {
	    	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	    	sdf.setLenient(false);
	    	try {
	    		Date d = sdf.parse(date);
	    		return true;
	    	} catch (ParseException e) {
	    		return false;
	    	}
    	}
    	return false;
	}
	
	/**
	 * This method is to validate cost information of a bike
	 * @param cost: cost information of a bike
	 * @return true/false
	 */
	public boolean validateCost(String cost) {
		//check the cost contains only number
    	try {
    		Integer.parseInt(cost);
    	} catch (NumberFormatException e) {
    		return false;
    	}
    	return true;
	}
	
	/**
	 * This method is to validate get information of a bike from database by id
	 * @param bikeId: id of a bike
	 * @return bike
	 */
	public Bike getBike(int bikeId) {
		Bike bike = new Bike();
		Connection conn = db.getConnection();
		try {
            PreparedStatement statement = conn.prepareStatement("select * from bike where id = ?");
            statement.setInt(1, bikeId);
            ResultSet rs= statement.executeQuery();
            if (rs.next()) {
                bike.setId(rs.getInt("id"));
                bike.setName(rs.getString("name"));
                bike.setType(rs.getInt("type"));
                bike.setWeight(rs.getInt("weight"));
                bike.setLicensePlate(rs.getString("licensePlate"));
                bike.setManuafacturingDate(rs.getString("manuafacturingDate"));
                bike.setProducer(rs.getString("producer"));
                bike.setCost(rs.getInt("cost"));
                bike.setStationId(rs.getInt("stationId"));
//               bike.setCreatedAt(rs.getDate("createdAt").toInstant());
//            	bike.setUpdatedAt(rs.getDate("updatedAt").toInstant());
            }
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
		return bike;
	}
}
