package exception;

public class InvalidInformationException extends BaseException {
	public InvalidInformationException(String message) {
		super(message);
	}
}
