package exception;

public class BikeExistedException extends BaseException {
	public BikeExistedException(String message) {
		super(message);
	}
}
