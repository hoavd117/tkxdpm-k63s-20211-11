package entity.common;

public class ElectricityBike extends Bike {
	private int batteryPercentage;
	private int loadCycles;
	private int usageTime;
	
	public ElectricityBike() {
		super();
		this.batteryPercentage = 0;
		this.loadCycles = 0;
		this.usageTime = 0;
	}
	
	public ElectricityBike(int batteryPercentage) {
		super();
		this.batteryPercentage = batteryPercentage;
		this.loadCycles = 0;
		this.usageTime = 0;
	}

	public int getBatteryPercentage() {
		return batteryPercentage;
	}

	public void setBatteryPercentage(int batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}

	public int getLoadCycles() {
		return loadCycles;
	}

	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}

	public int getUsageTime() {
		return usageTime;
	}

	public void setUsageTime(int usageTime) {
		this.usageTime = usageTime;
	}
	
	
}
