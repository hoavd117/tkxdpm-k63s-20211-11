package views.screen.return_bike;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;

import controller.ReturnBikeController;
import entity.common.Bike;
import entity.common.Transaction;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

public class ReturnBikeScreen extends BaseScreenHandler {
	private Transaction transaction;
	private Bike bike;
	
	@FXML
	Button btnBack;
	@FXML
	Text txtStartTime;
	@FXML
	Text txtReturnTime;
	@FXML
	Text txtTimeRent;
	@FXML
	Text txtFeeRent;
	@FXML
	Text txtDeposit;
	@FXML
	Text txtTotalRefund;
	
	@FXML
	Text txtBikeId;
	@FXML
	Text txtBikeDetail;
	@FXML
	Text txtBikeName;
	
	@FXML
	Button btnReturn;
	
	private ReturnBikeController bController;
	
	public ReturnBikeScreen(Stage stage, String screenPath, int transactionId) throws IOException {
		super(stage, screenPath);
		this.bController = new ReturnBikeController();
		initData(transactionId);
	}
	
	private void initData(int transactionId) {
		transaction = ((ReturnBikeController)bController).getTransanctionInfo(transactionId);
		Bike bike = ((ReturnBikeController)bController).getBikeInfo(transaction.getBikeId());
		
		txtStartTime.setText(transaction.getTimeStartRent().toString());
		txtReturnTime.setText(Instant.now().toString());
		
		long totalTimeRent = Duration.between(transaction.getTimeStartRent(), Instant.now()).toMinutes();
		txtTimeRent.setText(String.valueOf(totalTimeRent) + " minutes");
		
		long feeRent = ((ReturnBikeController)bController).calculateFeeRent(bike, totalTimeRent);
		txtFeeRent.setText(String.valueOf(feeRent) + " VND");
		
		txtDeposit.setText(String.valueOf(bike.getCost() + " VND"));
		txtTotalRefund.setText(String.valueOf(bike.getCost() - feeRent) + " VND");
		
		txtBikeId.setText(String.valueOf(bike.getId()));
		txtBikeDetail.setText(bike.getBikeDetail());
		txtBikeName.setText(bike.getName());
	}
	
	
	@FXML
	public void backPress() {
		this.stage.close();
	}
	
}
