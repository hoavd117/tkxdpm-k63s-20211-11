package main;
import constant.Const;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import views.screen.manage.AdministratorHomeScreenHandler;
import views.screen.return_bike.ReturnBikeScreen;

public class Main extends Application {
	
	@Override
	public void start(Stage primaryStage) throws Exception {
        //primaryStage.setTitle("ReturnBike");
        //ReturnBikeScreen returnBikeScreen = new ReturnBikeScreen(primaryStage, Const.ScreenPath.RETURN_BIKE_SCREEN_PATH, 1);
        //returnBikeScreen.show();
		primaryStage.setTitle("Administrator Home Screen");
        AdministratorHomeScreenHandler administratorHomeScreenHandler = new AdministratorHomeScreenHandler(primaryStage, Const.ScreenPath.ADMINISTRATOR_HOME_SCREEN_PATH);
        administratorHomeScreenHandler.show();
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
}
