package controller.return_bike;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.FeedbackController;
import controller.ProfileController;

public class ValidateMessageFeedbackTest {
	private FeedbackController feedbackController;
	
	@BeforeEach
	void setUp() throws Exception {
		feedbackController = new FeedbackController();
	}
	
	@ParameterizedTest
	@CsvSource({
		",false",
		"$#abc,false",
		"rat hai long,true",
	})

	//@Test
	void test(String message, boolean expected) {
		//when
		boolean isValided = feedbackController.validateMessage(message);
		
		//then
		assertEquals(expected, isValided);
	}
}
