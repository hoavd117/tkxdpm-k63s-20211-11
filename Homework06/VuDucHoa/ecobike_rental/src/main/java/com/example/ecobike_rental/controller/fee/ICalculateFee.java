package com.example.ecobike_rental.controller.fee;

import com.example.ecobike_rental.entity.BikeType;

import java.util.Date;

public interface ICalculateFee {
    long calculateFee(Date rentTime, Date returnTime, BikeType bikeType);
}
