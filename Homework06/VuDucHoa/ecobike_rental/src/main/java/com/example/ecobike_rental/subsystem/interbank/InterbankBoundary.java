package com.example.ecobike_rental.subsystem.interbank;


import com.example.ecobike_rental.common.exception.UnrecognizedException;
import com.example.ecobike_rental.utils.API;

public class InterbankBoundary {

	String query(String url, String data) {
		String response = null;
		try {
			response = API.post(url, data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new UnrecognizedException();
		}
		return response;
	}

}
