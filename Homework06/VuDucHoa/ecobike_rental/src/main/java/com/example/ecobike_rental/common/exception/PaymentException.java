package com.example.ecobike_rental.common.exception;;

public class PaymentException extends RuntimeException {
	public PaymentException(String message) {
		super(message);
	}
}
