package com.example.ecobike_rental.controller.fee;

import com.example.ecobike_rental.entity.BikeType;

import java.util.Date;

public class NewCalculateFee implements ICalculateFee {
    @Override
    public long calculateFee(Date rentTime, Date returnTime, BikeType bikeType) {
        long price = calculateFeeWithoutBikeType(rentTime, returnTime);

        if(bikeType == BikeType.NormalBike)
            return price;
        if(bikeType == BikeType.DoubleBike || bikeType == BikeType.ElectricBike){
            return (long)(1.5 * price);
        }
        return 0;
    }

    private long calculateFeeWithoutBikeType(Date rentTime, Date returnTime) {
        long minutes = (returnTime.getTime() - rentTime.getTime()) / 60000;

        long price = 200000;

        // Nếu khách hàng trả xe sau khi chỉ thuê xe dưới 12 tiếng, khách hàng sẽ
        // được hoàn tiền 10.000VNĐ mỗi tiếng trả sớm.
        long hour = minutes / 60;
        if(hour < 12) {
            return price - (12 - minutes) * 10000;
        }

        // Nếu khách hàng trả xe sau khi mượn từ tiếng thứ 12 trở đi, khách hàng
        // sẽ không được hoàn tiền
        if(hour < 24) {
            return price;
        }

        // Nếu khách hàng trả xe muộn quá thời gian thỏa thuận, cứ mỗi 15 phút
        // muộn, khách sẽ phải trả thêm 2.000 đồng.
        minutes = minutes - 24 * 60;
        return price + (minutes/15)*2000;
    }
}
