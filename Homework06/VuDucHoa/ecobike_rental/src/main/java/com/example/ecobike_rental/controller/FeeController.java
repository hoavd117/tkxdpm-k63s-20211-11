package com.example.ecobike_rental.controller;

import com.example.ecobike_rental.controller.fee.CurrentCalculateFee;
import com.example.ecobike_rental.controller.fee.ICalculateFee;
import com.example.ecobike_rental.controller.fee.NewCalculateFee;
import com.example.ecobike_rental.entity.BikeType;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FeeController {
    @FXML
    public Label feeValue;
    public TextField returnTimeField;
    public TextField rentTimeField;
    public ChoiceBox bikeTypeChoiceBox;
    ICalculateFee calculateFee = new CurrentCalculateFee();
    int toggled = 0;

    @FXML
    ToggleGroup group;

    @FXML
    private RadioButton currentCalculateFeeButton;

    @FXML
    private RadioButton newCalculateFeeButton;

    @FXML
    protected void onCurrentCalculateFeeButtonButtonClick() {
        toggled = 0;
    }

    @FXML
    protected void onNewCalculateFeeButtonButtonClick() {
        toggled = 1;
    }

    @FXML
    protected  void onChangeFeeButtonClick() {
        if(toggled == 0) {
            calculateFee = new CurrentCalculateFee();
        } else{
            calculateFee = new NewCalculateFee();
        }
    }

    @FXML
    public void onCalculateFeeClick() {
        var rentTime = stringToDateTime(rentTimeField.getText());
        var returnTime = stringToDateTime(returnTimeField.getText());
        var bikeTypeValue = bikeTypeChoiceBox.getValue();
        BikeType bikeType = BikeType.NormalBike;
        if ("Xe đạp thường".equals(bikeTypeValue)) {
            bikeType = BikeType.NormalBike;
        } else if ("Xe đạp đôi".equals(bikeTypeValue)) {
            bikeType = BikeType.DoubleBike;
        } else if ("Xe đạp điện".equals(bikeTypeValue)) {
            bikeType = BikeType.ElectricBike;
        }

        long price = calculateFee.calculateFee(rentTime, returnTime, bikeType);
        feeValue.setText(price + "đ");
    }

    Date stringToDateTime(String dateString) {
        try {
            DateFormat dateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy");
            Date date = dateFormat.parse(dateString);
            return date;
        } catch (ParseException e) {
            return new Date();
        }
    }
}