package com.example.ecobike_rental.controller.fee;

import com.example.ecobike_rental.entity.BikeType;

import java.util.Date;

public class CurrentCalculateFee implements ICalculateFee {

    @Override
    public long calculateFee(Date rentTime, Date returnTime, BikeType bikeType) {
        long price = calculateFeeWithoutBikeType(rentTime, returnTime);

        if(bikeType == BikeType.NormalBike)
            return price;
        if(bikeType == BikeType.DoubleBike || bikeType == BikeType.ElectricBike){
            return (long)(1.5 * price);
        }
        return 0;
    }

    private long calculateFeeWithoutBikeType(Date rentTime, Date returnTime) {
        long minutes = (returnTime.getTime() - rentTime.getTime()) / 60000;

        // Nếu 10 phút đều thì miễn phí
        if(minutes < 10)
            return 0;

        // Giá khởi điểm cho 30 phút đầu là 10.000 đồng
        if(minutes < 30){
            return 10000;
        }

        // Cứ mỗi 15 phút tiếp theo, khách sẽ phải trả thêm 3.000 đồng
        minutes = minutes - 30;
        return 10000 + 3000 * minutes/15;
    }
}
