package com.example.ecobike_rental.entity;

public enum BikeType {
    NormalBike,
    DoubleBike,
    ElectricBike,
}
