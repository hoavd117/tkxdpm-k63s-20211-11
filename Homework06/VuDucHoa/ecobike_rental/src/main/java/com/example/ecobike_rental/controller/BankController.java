package com.example.ecobike_rental.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class BankController {
    @FXML
    private TextField cardCodeField;
    @FXML
    private TextField ownerField;
    @FXML
    private TextField dateExpiredField;
    @FXML
    private TextField cvvCodeField;
    @FXML
    private Label result;
    @FXML
    private Label message;

    @FXML
    protected void onDepositButtonClick() {
        String cardCode = cardCodeField.getText();
        String owner = ownerField.getText();
        String dateExpired = dateExpiredField.getText();
        String cvvCode = cvvCodeField.getText();

        var paymentController = new PaymentController();
        var response =  paymentController.payOrder(700000, "", cardCode, owner, dateExpired, cvvCode);
        result.setText(response.get("RESULT"));
        message.setText(response.get("MESSAGE"));

    }
}
