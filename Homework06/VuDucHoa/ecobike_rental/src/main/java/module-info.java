module com.example.ecobike_rental {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;


    opens com.example.ecobike_rental to javafx.fxml;
    exports com.example.ecobike_rental;
    exports com.example.ecobike_rental.controller;
    opens com.example.ecobike_rental.controller to javafx.fxml;
    exports com.example.ecobike_rental.controller.fee;
    opens com.example.ecobike_rental.controller.fee to javafx.fxml;
}